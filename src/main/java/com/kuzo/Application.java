package com.kuzo;

import com.kuzo.passive.view.MyView;

/**
 * executive class.
 */
public class Application {
    /**
     *  entry point of the application.
     * @param args system requirements.
     */
    public static void main(final String[] args) {
        new MyView().show();
    }
}
