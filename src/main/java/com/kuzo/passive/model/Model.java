package com.kuzo.passive.model;

import com.kuzo.passive.chemicalgoods.ChemicalGoods;

import java.util.ArrayList;

public interface Model {
     ArrayList<ChemicalGoods> getGoodsList();
     void sortByClassName();
     void addElement(ChemicalGoods goods);
     void removeAllElements();
     ArrayList<ChemicalGoods> findInPrice(Class ChemicalGoods, int min, int max);

}
