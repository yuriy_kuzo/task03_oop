package com.kuzo.passive.model;

import com.kuzo.passive.chemicalgoods.ChemicalGoods;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * there are model logic.
 */
public class HouseholdChemicals implements Model {
    private ArrayList<ChemicalGoods> ChemicalGoodsList;
    public HouseholdChemicals() {
        super();
        ChemicalGoodsList = new ArrayList<ChemicalGoods>();
    }
    @Override
    /**
     * get the GoodsList.
     */
    public ArrayList<ChemicalGoods> getGoodsList() {
        return ChemicalGoodsList;
    }

    /**
     * sorting by Name logic.
     */
    @Override
    public void sortByClassName() {
        ChemicalGoodsList.sort(Comparator.comparingInt(ChemicalGoods::getClassNumber));
    }

    /**
     * add goods to list.
     * @param goods goods
     */
    @Override
    public void addElement(ChemicalGoods goods) {
        ChemicalGoodsList.add(goods);
    }

    /**
     * clear all elements from list.
     */
    @Override
    public void removeAllElements() {
        ChemicalGoodsList.clear();
    }

    /**
     * searching by price logic.
     * @param ChemicalGoods chemical goods
     * @param min minimum price
     * @param max maximum price
     * @return goods in present price interval
     */
    @Override
    public  ArrayList<ChemicalGoods> findInPrice (Class ChemicalGoods, final int min, final int max) {
        ArrayList<ChemicalGoods> goodsInPrice = new ArrayList<>();
        for (ChemicalGoods g : ChemicalGoodsList) {
            if (g.getClass() == ChemicalGoods && g.getPrice() > min && g.getPrice() < max) goodsInPrice.add(g);
        }
        return goodsInPrice;
    }
}
