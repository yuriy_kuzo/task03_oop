package com.kuzo.passive.view;

import com.kuzo.passive.controller.Controller;
import com.kuzo.passive.controller.Controllerlmpl;
import com.kuzo.passive.chemicalgoods.ChemicalGoods;
import com.kuzo.passive.chemicalgoods.Cleaners;
import com.kuzo.passive.chemicalgoods.Dishwashers;
import com.kuzo.passive.chemicalgoods.Laundry;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView implements View {
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input;

    public MyView() {
        controller = new Controllerlmpl();
        input = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - print list of all chemical goods");
        menu.put("2", "  2 - update list");
        menu.put("3", "  3 - find chemical goods");
        menu.put("4", "  4 - sort list");
        menu.put("E", "  E - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
    }
    private void pressButton1() {
        for (ChemicalGoods g : controller.getGoodsList()) {
            System.out.println(g);
        }
    }
    @Deprecated
    private void pressButton2() {
        try {
            controller.updateList();
        } catch (IOException e) {
            e.printStackTrace();
        }
        pressButton1();
    }
    private void pressButton3() {
        System.out.println("Please input number of chemical goods type you want to find:");
        System.out.print("1-Cleaners 2-Dishwashers 3-Laundry: ");
        int typeNumber = Integer.parseInt(input.nextLine());
        System.out.print("Enter 2 numbers: maximum and minimum price.");
        int minPrice = Integer.parseInt(input.nextLine());
        int maxPrice = Integer.parseInt(input.nextLine());
        switch (typeNumber) {
            case 1:
                System.out.println(controller.findInPrice(Cleaners.class, minPrice, maxPrice));
                break;
            case 2:
                System.out.println(controller.findInPrice(Dishwashers.class, minPrice, maxPrice));
                break;
            case 3:
                System.out.println(controller.findInPrice(Laundry.class, minPrice, maxPrice));
                break;
            default:
                System.out.println("Fail input. Try again");
                return;

        }
    }
    private void pressButton4() {
        controller.sortGoodsListByName();
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) { }
        } while (!keyMenu.equals("E"));
    }
    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }
}
