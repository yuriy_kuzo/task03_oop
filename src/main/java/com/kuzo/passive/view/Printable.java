package com.kuzo.passive.view;

@FunctionalInterface
public interface Printable {
        void print();
}
