package com.kuzo.passive.controller;

import com.kuzo.passive.chemicalgoods.ChemicalGoods;
import com.kuzo.passive.chemicalgoods.Cleaners;
import com.kuzo.passive.chemicalgoods.CleanersType;
import com.kuzo.passive.chemicalgoods.Dishwashers;
import com.kuzo.passive.chemicalgoods.DishwashersType;
import com.kuzo.passive.chemicalgoods.Laundry;
import com.kuzo.passive.chemicalgoods.LaundryType;
import com.kuzo.passive.model.HouseholdChemicals;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
/**
 * class with business logic.
 */
public class Controllerlmpl implements Controller {
    private HouseholdChemicals householdChemicals;
    public Controllerlmpl() {
        householdChemicals = new HouseholdChemicals();
    }
    /**
     * search goods by their class(type) and price interval.
     * @param Goods class of goods
     * @param min minimum price of the goods
     * @param max maximum price of the goods
     * @return goods, min and max price
     */
    @Override
    public  List<ChemicalGoods> findInPrice (Class Goods, final int min, final int max) {
        return householdChemicals.findInPrice(Goods, min, max);
    }
    /**
     * access to list of the goods.
     * @return goods list
     */
    @Override
    public List<ChemicalGoods> getGoodsList() {
        return  householdChemicals.getGoodsList();
    }
    /**
     * execute sorting by name.
     */
    @Override
    public void sortGoodsListByName() {
        householdChemicals.sortByClassName();
    }
    /**
     * read goods from ChemicalGoods.txt file.
     * @throws IOException exception throw
     */
    @Override
    public void updateList() throws IOException {
        householdChemicals.removeAllElements();
        readFromFile();
    }
    /**
     * reader each type of goods.
     * className is 1st element in array
     * depend on which refer to certain type of goods
     * @throws IOException - exception throw
     */
    @Override
    public void readFromFile() throws IOException {
        File file = new File("ChemicalGoods.txt");
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String line = null;
        try {
            String className = null;
            while ((line = reader.readLine()) != null) {
                String[] lineArray = line.split(" ");
                className = lineArray[0];

                switch (className) {

                    case "Laundry":
                        LaundryType laundryType = LaundryType.valueOf(lineArray[1]);
                        try {
                            householdChemicals.addElement(new Laundry(laundryType,
                                    Integer.valueOf(lineArray[2]), Double.valueOf(lineArray[3])));

                        } catch (IllegalArgumentException e) {
                            e.printStackTrace();
                            System.out.println("Check file ChemicalGoods.txt");
                        }
                        break;

                    case "Dishwashers":
                        DishwashersType dishwashersType = DishwashersType.valueOf(lineArray[1]);
                        try {
                            householdChemicals.addElement(new Dishwashers(dishwashersType,
                                    Integer.valueOf(lineArray[2]), Double.valueOf(lineArray[3])));

                        } catch (IllegalArgumentException e) {
                            e.printStackTrace();
                            System.out.println("Check file ChemicalGoods.txt");
                        }
                        break;

                    case "Cleaners":
                        CleanersType cleanersType = CleanersType.valueOf(lineArray[1]);
                        try {
                            householdChemicals.addElement(new Cleaners(cleanersType, Integer.valueOf(lineArray[2]),
                                    Double.valueOf(lineArray[3])));

                        } catch (IllegalArgumentException e) {
                            e.printStackTrace();
                            System.out.println("Check file ChemicalGoods.txt");
                        }
                        break;
                    default:
                        System.out.println("Wrong file line!");
                        break;
                }
            }
        } finally {
            reader.close();
        }
    }
}
