package com.kuzo.passive.controller;

import com.kuzo.passive.chemicalgoods.ChemicalGoods;

import java.io.IOException;
import java.util.List;

/**
 * interface allows to manipulate goods list.
 */
public interface Controller {
     List<ChemicalGoods> getGoodsList();
     void sortGoodsListByName();
     void updateList() throws IOException;
      List<ChemicalGoods> findInPrice(Class Goods, int min, int max);
     void readFromFile()  throws IOException;;
}
