package com.kuzo.passive.chemicalgoods;
/**
 * class represent Laundry type of good.
 */
public class Laundry extends ChemicalGoods {
    public Laundry() {
        super();
    }
    /**
     *  create the Laundry object with parameters.
     * @param cleanersType instance of the LaundryType class
     * @param price price of the goods
     * @param volume volume of the goods
     */
    private LaundryType laundryType;
    public Laundry(LaundryType laundryType, final int price, final double weight) {
        super(price, weight);
        this.laundryType = laundryType;
    }
    /**
     * @return type of the Laundry goods.
     */
    @Override
    public String toString() {
        return  super.toString() + "Type: " + laundryType;
    }
}
