package com.kuzo.passive.chemicalgoods;
/**
 * class represent Cleaners type of good.
 */
public class Dishwashers extends ChemicalGoods {
    public Dishwashers() {
        super();
    }
    /**
     *  create the Dishwashers object with parameters.
     * @param cleanersType instance of the DishwashersType class
     * @param price price of the goods
     * @param volume volume of the goods
     */
    private DishwashersType dishwashersType;
    public Dishwashers(DishwashersType dishwashersType, final int price, final double volume) {
        super(price, volume);
        this.dishwashersType = dishwashersType;
    }
    /**
     * @return type of the Dishwashers goods.
     */
    @Override
    public String toString() {
        return  super.toString() + "Type: " + dishwashersType;
    }
}
