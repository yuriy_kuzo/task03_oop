package com.kuzo.passive.chemicalgoods;
/**
 * enum of the DishwashersType elements.
 */
public enum DishwashersType {
    Powder, HandwashLiquid, RinseAgents
}
