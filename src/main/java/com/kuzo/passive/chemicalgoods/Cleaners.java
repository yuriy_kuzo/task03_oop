package com.kuzo.passive.chemicalgoods;

/**
 * class represent Cleaners type of good.
 */
public class Cleaners extends ChemicalGoods {
    public Cleaners() {
        super();
    }
    private CleanersType cleanersType;

    /**
     *  create the Cleaners object with parameters.
     * @param cleanersType instance of the CleanersType class.
     * @param price price of the goods
     * @param volume volume of the goods
     */
    public Cleaners(CleanersType cleanersType, int price, double volume) {
        super(price, volume);
        this.cleanersType = cleanersType;
    }

    /**
     * @return type of the Cleaners goods.
     */
    @Override
    public String toString() {
        return  super.toString() + "Type: " + cleanersType;
    }
}
