package com.kuzo.passive.chemicalgoods;

/**
 * enum of the CleanersType elements.
 */
public enum CleanersType {
    Glass, Floors, Wipes
}
