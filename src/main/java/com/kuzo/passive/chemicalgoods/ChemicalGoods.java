package com.kuzo.passive.chemicalgoods;

/**
 * create the abstract class, which is parental.
 * to all other chemical goods.
 */
public abstract class ChemicalGoods  {
    /**
     * declare price parameter.
     */
    private int price;
    /**
     * declare volume parameter.
     */
    private double volume;

    public ChemicalGoods() {
        price = 0;
        volume = 0;
    }
    ChemicalGoods(final int extPrice,final double extVolume) {
        price = extPrice;
        volume = extVolume;
    }
    /**
     * @return line, that output main parameters of the class(object).
     */
    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " Price: " + price
                + "  Volume: " + volume + " ml  ";
    }
    /**
     * @return number, which responds to type of chemical goods.
     */
    public int getClassNumber() {
        switch (this.getClass().getSimpleName()) {
            case "Cleaners": return 0;
            case "Dishwashers": return 1;
            case "Laundry": return 2;
            default: return 3;
        }
    }
    /**
     * @return price of the goods.
     */
    public int getPrice() {
        return price;
    }
}


