package com.kuzo.passive.chemicalgoods;
/**
 * enum of the LaundryType elements.
 */
public enum  LaundryType {
    Bleach, PodDetergent, StainRemover
}
